# Archiv der Veranstaltungen der NExT Community Cloud für die Verwaltung

| Datum | Titel | Vortragende | Institution | Folien |
|-----------------------|------------------------|----|----|----|
| 05.06.2024 | Austausch zur MultiCloud der Bundeswehr | Peter Ehrle | Zentrum Digitalisierung der Bundeswehr und Fähigkeitsentwicklung CIR | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/Ehrle%20-%20Austausch%20zur%20MultiCloud%20der%20Bundeswehr.pdf?ref_type=heads)|
| 11.04.2024 | OpenDesk ZenDiS  |    | ZenDiS GmbH | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/opendesk-zendis.pdf)|
| 17.01.2024 | Cloud Beschaffung  | Alexandros Bouras   | DWD | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/DWD_Cloud_Beschaffung.pdf?ref_type=heads)|
| 17.01.2024 | CaaS  |   | ITZBund | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/DerCaaSImpuls.pdf?ref_type=heads)|
| 06.09.2023 | Multicloud  | Harald Joos    |Deutsche Rentenversicherung Bund | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/2023_Multi-Cloud_V38G.pdf?ref_type=heads)|
| 06.09.2023 | Cloud Marktplätze  | Jens Giere    | PD – Berater der öffentlichen Hand GmbH | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/20230904_Cloud-Marktpl%C3%A4tze.pdf?ref_type=heads)|
| 06.09.2023 | Deutsche Verwaltungscloud Strategie | Matthias Burgfried  | BMI | [Folien](https://gitlab.opencode.de/next-netz/cloud-community/cloud-fuer-die-verwaltung/-/blob/main/Archiv%20der%20Veranstaltungen/Folien/Burgfried%20-%20Deutsche%20Verwaltungscloud%20Strategie.pdf?ref_type=heads)|

